import React, {useState} from 'react'
import {View, TextInput, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const TextInputPassword = () => {
    const [showPassword, setShowPassword] = useState(false);

    const iconeye = !showPassword ? 'eye' : 'eye-slash'

    const changeShowPassword = () => {
        setShowPassword(!showPassword)
    }

    return(
        <View style={{position: 'relative'}}>
            <TextInput style={styles.textInput} placeholder='Password' secureTextEntry={showPassword} />
            <Icon name={iconeye} size={20} color="#95a5a6" style={styles.iconeye} onPress={changeShowPassword} />
        </View>
    )
}

const styles = StyleSheet.create({
    textInput : {
        padding: 10,
        borderWidth:1,
        borderColor: '#000',
        marginTop: 10,
        fontFamily: 'Poppins-Reguler',
        borderRadius:10
    },
    iconeye: {
        position: 'absolute',
        textAlign: 'center',
        textAlignVertical: 'center',
        marginTop: 10,
        top: 10,
        right: 15
    }
})

export default TextInputPassword