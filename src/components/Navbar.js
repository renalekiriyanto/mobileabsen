import { StyleSheet, Text, View } from 'react-native';
import React from 'react';

const Navbar = () => {
  return (
    <View style={styles.navbar}>
      <Text style={styles.navbarText}>Navbar</Text>
    </View>
  );
};


const styles = StyleSheet.create({
  navbar: {
    backgroundColor: '#95a5a6',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  navbarText: {
      color: '#fff',
      fontWeight: 'bold',
      fontSize: 16
  }
});

export default Navbar;