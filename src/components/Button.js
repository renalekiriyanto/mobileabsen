import React from 'react';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const Button = (props) => {
    return(
        <TouchableOpacity
            onPress={() => console.log('diklik dong')}
        >
            <View
                style={styles.container}
            >
                {props.label}
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#3498db',
        width: 150,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
    }
})

export default Button