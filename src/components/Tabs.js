import React from 'react'
import {View, Text} from 'react-native'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Icon from 'react-native-vector-icons/FontAwesome';

// components
import Beranda from '../pages/Beranda'
import Profile from '../pages/Profile'
import Settings from '../pages/Settings'
import Attendence from '../pages/Attendence'

const Tab = createBottomTabNavigator()

const Tabs = () => {
    return(
        <Tab.Navigator
            screenOptions={{
                tabBarShowLabel: false,
                tabBarActiveTintColor: '#3498db',
                tabBarActiveTintColor: '#2980b9',
            }}
        >
            <Tab.Screen
                options={{
                    tabBarIcon: ({focused}) => (
                        <View style={{alignItems:'center', justifyCenter: 'center', top:10}}>
                            <Icon name="home" size={25} style={{
                                marginBottom:25, color: focused ? '#2980b9' : '#bdc3c7'
                            }} />
                        </View>
                    ),
                }}
                name="Beranda" component={Beranda} />

            <Tab.Screen
            options={{
                tabBarIcon: ({focused}) => (
                    <View style={{alignItems:'center', justifyCenter: 'center', top:10}}>
                        <Icon name="file" size={20} style={{
                                marginBottom:25, color: focused ? '#2980b9' : '#bdc3c7'
                            }} />
                    </View>
                )
            }}
            name="Kehadiran" component={Attendence} />

            <Tab.Screen
                options={{
                    tabBarIcon: ({focused}) => (
                        <View style={{alignItems:'center', justifyCenter: 'center', top:10}}>
                            <Icon name="user" size={25} style={{
                                marginBottom:25, color: focused ? '#2980b9' : '#bdc3c7'
                            }} />
                        </View>
                    ),
                }}
                name="Profil" component={Profile} />

            <Tab.Screen
            options={{
                tabBarIcon: ({focused}) => (
                    <View style={{alignItems:'center', justifyCenter: 'center', top:10}}>
                        <Icon name="cog" size={25} style={{
                                marginBottom:25, color: focused ? '#2980b9' : '#bdc3c7'
                            }} />
                    </View>
                )
            }}
            name="Pengaturan" component={Settings} />
        </Tab.Navigator>
    )
}

export default Tabs