import React from 'react'
import {View, Text, TextInput, StyleSheet} from 'react-native'
// compoenents
import Button from '../components/Button'

const CreateRoom = () => {
    return(
        <View style={styles.container}>
            <Text style={styles.header}>Buat Room</Text>
            <TextInput placeholder="Nama Room" style={styles.textInput} />
            <View style={styles.containerButton}>
                <Button label={<Text style={styles.text}>Generate Room</Text>} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding:20
    },
    header: {
        fontFamily: 'Poppins-Reguler',
        fontWeight: '500',
        fontSize: 24,
        color: '#000'
    },
    textInput: {
        fontFamily: 'Poppins-Reguler',
        color: '#000',
        borderColor: '#000',
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        marginTop: 15,
    },
    text: {
        fontFamily: 'Poppins-Reguler',
        fontSize: 15,
        color: '#fff',
        fontWeight: '500',
    },
    containerButton:{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    }
})

export default CreateRoom