import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
// components
import Button from '../components/Button'

const GenerateRoom = () => {
    return(
        <View style={styles.container}>
            <View>
                <Text style={styles.header}>Generate Room</Text>
                <Text style={styles.text}>U0ROIDAyNSBTaW5hbWJlaw==</Text>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center', alignItems: 'center',
                }}>
                    <TouchableOpacity
                        style={{
                            marign:25
                        }}
                    >
                        <Icon name="whatsapp" size={30} color='#2ecc71' />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            margin: 25
                        }}
                    >
                        <Icon name="copy" size={30} color='#7f8c8d' />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{marginTop: 200, justifyContent: 'center', alignItems: 'center'}}>
                <TouchableOpacity>
                    <Icon name="home" size={50} color='#2980b9'/>
                </TouchableOpacity>
                <Text style={{
                    fontFamily: 'Poppins-Reguler',
                    fontSize: 25,
                    fontWeight: '500',
                    textAlign: 'center'
                }}>Beranda</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center', alignItems: 'center',
        padding: 100
    },
    header: {
        fontFamily: 'Poppins-Reguler',
        fontWeight: '500',
        color: '#000',
        fontSize: 18,
    },
    text: {
        fontFamily: 'Poppins-Reguler',
        fontWeight: 'bold',
        fontSize: 25,
        textAlign: 'center',
        marginTop: 25,
        color: '#000'
    }
})

export default GenerateRoom