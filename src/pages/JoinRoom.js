import React from 'react'
import {View, TextInput, Text, StyleSheet} from 'react-native'
import Button from '../components/Button'

const JoinRoom = () => {
    return(
        <View style={styles.container}>
            <Text style={styles.header}>Join Room</Text>
            <TextInput style={styles.textInput} placeholder="Masukkan Kode Room..." />
            <View style={styles.containerButton}>
                <Button label={<Text style={styles.text}>Gabung Room</Text>} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        padding: 25
    },
    header: {
        fontFamily: 'Poppins-Reguler',
        fontWeight: '500',
        fontSize: 20,
    },
    textInput: {
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 10,
        padding: 10,
        color: '#000',
        fontFamily: 'Poppins-Reguler',
    },
    containerButton: {
        justifyContent: 'center', alignItems: 'center',
        marginTop: 25
    },
    text: {
        fontFamily: 'Poppins-Reguler',
        color: '#fff',
        fontWeight: '500',
    }
})

export default JoinRoom