import React from 'react'
import {View, Text, Image, TouchableOpacity, StyleSheet, Dimensions} from 'react-native'
import Button from '../components/Button'

const AfterSplash = () => {
    return(
        <View style={{
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 20
        }}>
            <View style={{
                backgroundColor:'#ECF0F1',
                width: 350,
                height: 350,
                borderRadius: 10,
                marginTop: 20
            }}>
                <Image
                    source={require('../attribute/img/touch-g455d66535_1280.png')}
                    style={{
                        width: 350,
                        height: 350,
                        borderRadius: 10
                    }}
                    resizeMode='cover'
                />
            </View>
            <View
                style={{
                    width: Dimensions.get('window').width,
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center'
                }}
            >
                <TouchableOpacity
                    style={{
                        width: 100,
                        height: 50,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 10
                    }}
                >
                    <Text
                        style={{
                            fontFamily: 'Poppins-Reguler',
                            color: '#2C3E50',
                            fontWeight: '700'
                        }}
                    >Login</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{
                        backgroundColor: '#0077C0',
                        width: 100,
                        height: 50,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 10
                    }}
                >
                    <Text
                        style={{
                            fontFamily: 'Poppins-Reguler',
                            color: '#f5f6fa',
                            fontWeight: '700'
                        }}
                    >Register</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles= StyleSheet.create({

})

export default AfterSplash