import React, {useState, useRef} from 'react';
import {View, Text, StyleSheet, TextInput, Switch, TouchableOpacity, SafeAreaView, Dimensions} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/FontAwesome';
// component
import Button from '../components/Button'
import TextInputPassword from '../components/TextInputPassword';

const Register = () => {
    const [isEnabled, setIsEnabled] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
      {label: 'Kepala Sekolah', value: 'Kepala Sekolah'},
      {label: 'Operator', value: 'Operator'},
      {label: 'Guru', value: 'Guru'},
    ]);

    const changeModalVisibility = (bool) => {
        setIsModalVisible(bool);
    }

    const toggleSwitch =() => {
        setIsEnabled(!isEnabled);
    }

    return(
        <View>
            <View style={styles.container}>
                <Text style={styles.teks}>Pendaftaran Akun</Text>
                <View style={{
                    marginTop:20
                }}>
                    <TextInput style={styles.textInput} placeholder='E-mail' />
                    <TextInputPassword />
                    <TextInput style={styles.textInput} placeholder='Nama Lengkap' />
                    <TextInput style={styles.textInput} placeholder='No HP' />
                    <TextInput style={isEnabled ? styles.hidden : styles.textInput} placeholder='Whatsapp' />

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        marginTop: 10
                    }}
                    >
                        <Text style={{fontFamily: 'Poppins-Reguler'}}>No Whatsapp sama dengan No.HP</Text>
                        <Switch
                            trackColor={{ false: "#767577", true: "#81b0ff" }}
                            thumbColor={isEnabled ? "#2980b9" : "#f4f3f4"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={toggleSwitch}
                            value={isEnabled}
                        />
                    </View>

                    <DropDownPicker
                        open={open}
                        value={value}
                        items={items}
                        setOpen={setOpen}
                        setValue={setValue}
                        setItems={setItems}
                        zIndexInverse={1000}
                        placeholder="Jabatan..."
                        textStyle={{fontFamily: 'Poppins-Reguler'}}
                        listMode='MODAL'
                        searchContainerStyle={{
                            borderBottomColor: "#dfdfdf"
                        }}
                    />

                    {/* button */}
                    <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 150,}}>
                        <Button label={<Icon name="angle-double-right" size={30} color='#ecf0f1' />} />
                    </View>
                </View>
            </View>
        </View>
    )
}

export default Register;

const styles = StyleSheet.create({
    container: {
        padding:15,
        justifyContent: 'center',
        alignContent:'center'
    },
    teks: {
        fontSize: 20,
        fontFamily: 'Poppins-Reguler',
        color: '#000'
    },
    textInput: {
        padding: 10,
        borderWidth:1,
        borderColor: '#000',
        marginTop: 5,
        fontFamily: 'Poppins-Reguler',
        borderRadius:10
    },
    hidden: {
        display: 'none',
        padding: 10,
        borderWidth:1,
        borderColor: '#34495e',
        marginTop: 5
    },
    iconeye: {
        position: 'absolute',
        textAlign: 'center',
        textAlignVertical: 'center',
        marginTop: 10,
        top: 10,
        right: 15
    }
})