import React from 'react'
import{View, Text,TextInput, StyleSheet} from 'react-native'
// components
import TextInputPassword from '../components/TextInputPassword'
import Button from '../components/Button'

const Login = () => {
    return(
        <View style={styles.container}>
            <Text style={styles.header}>Login</Text>
            <TextInput style={styles.textInput} placeholder="E-mail" />
            <TextInputPassword />
            <View style={styles.containerButton}>
                <Button label={<Text style={styles.text}>Login</Text>} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    header: {
        fontSize: 32,
    },
    textInput: {
        borderWidth: 1,
        borderColor: '#000',
        borderRadius:10,
        padding: 10,
        color: '#000',
        fontFamily: 'Poppins-Reguler',
        marginTop: 10
    },
    text: {
        fontFamily: 'Poppins-Reguler',
        color: '#fff',
        fontSize: 20
    },
    containerButton:{
        justifyContent: 'center', alignItems: 'center',
        marginTop: 10
    }
})

export default Login