import React from 'react'
import {View, Text, Image, FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';


const DATA = [
    {
        key: 1,
        label: 'Hapus Akun',
        icon: 'user-times',
    },
    {
        key: 2,
        label: 'Keluar',
        icon: 'sign-out',
    },
]

const Item = ({label, icon}) => (
    <View style={{
        flexDirection: 'row',
    }}>
        <View
            style={{
                width: 30,
                height: 30,
                marginRight: 10
            }}
        >
            <Icon name={icon} size={25} color='#7f8c8d' />
        </View>
        <View>
            <Text
                style={{
                    fontFamily: 'Poppins-Reguler',
                    fontSize: 18,
                }}
            >{label}</Text>
        </View>
    </View>
)

const Settings = () => {
    const renderItem = ({item}) => (
        <Item label={item.label} icon={item.icon} />
    )

    return(
        <View style={{
            padding:20,
            backgroundColor: '#ecf0f1',
            justifyContent: 'center',
            alignItems: 'center',
        }}>
            <View style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: 200,
                height: 200,
                backgroundColor: '#3498db',
                borderRadius: 25
            }}>
                <View style={{
                    backgroundColor: '#fff',
                    padding: 4,
                    borderRadius: 50
                }}>
                    <Image
                        source={require('../attribute/img/profile.jpg')}
                        style={{
                            width: 100,
                            height: 100,
                            borderRadius: 50
                        }}
                        resizeMode='cover'
                    />
                </View>
                <Text
                    style={{
                        marginTop: 10,
                        fontFamily: 'Poppins-Reguler',
                        fontSize: 18,
                        color: '#ecf0f1'
                    }}
                >Renal Eki Riyanto</Text>
            </View>
            <View
                style={{
                    marginTop: 10,
                    backgroundColor: '#ecf0f1',
                    width: 300,
                }}
            >
                <FlatList
                    data={DATA}
                    renderItem={renderItem}
                    keyExtractor={item => item.key}
                />
            </View>
        </View>
    )
}

export default Settings