import React from 'react'
import {View, Text} from 'react-native'

const Beranda = () => {
    return(
        <View style={{
        justifyContent: 'center', alignItems: 'center'
        }}
        >
        <View style={{
            width: 100,
            height: 100,
            backgroundColor: '#859596',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 5,
            borderRadius: 10,
        }}>
            <Text style={{
            color: '#D5E5E6',
            fontWeight: 'bold',
            fontFamily: 'Poppins-Reguler',
            }}>Gambar Disini</Text>
        </View>
        </View>
    )
}

export default Beranda
