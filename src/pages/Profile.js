import React, {useState} from 'react'
import {View, Text, Image, TouchableOpacity, Dimensions, StyleSheet, Switch } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';


const Profile = ({navigation}) => {

    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);

    return (
        <View style={{
            backgroundColor: '#c8d6e5',
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1
        }}>
            <View style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
                padding: 10
            }}>
                <View style={{
                    backgroundColor: '#10ac84',
                    padding: 4,
                    borderRadius: 50
                }}>
                    <Image
                        source={require('../attribute/img/profile.jpg')}
                        style={{
                            width: 100,
                            height: 100,
                            borderRadius: 50
                        }}
                        resizeMode='cover'
                    />
                </View>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Text
                        style={{
                            marginTop: 10,
                            fontFamily: 'Poppins-Reguler',
                            fontSize: 18,
                            color: '#222f3e'
                        }}
                    >Renal Eki Riyanto</Text>
                    <TouchableOpacity
                        style={{
                            backgroundColor: '#341f97',
                            justifyContent: 'center',
                            alignItems: 'center',
                            padding: 10,
                            borderRadius: 15
                        }}
                    >
                        <Text
                            style={{
                                color: '#fff',
                                fontFamily: 'Poppins-Reguler',
                                fontSize: 12,
                                fontWeight: '500',
                            }}
                        >Ubah Profil</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{padding: 10, width: Dimensions.get('window').width, flex: 1 }}>
                <View
                    style={{
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        flexDirection: 'row',
                        height: 55
                    }}
                >
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Icon name="adjust" size={25} style={{marginRight: 5}} />
                        <Text style={styles.textMenu}>Mode Gelap</Text>
                    </View>
                    <Switch
                        trackColor={{ false: "#767577", true: "#81b0ff" }}
                        thumbColor={isEnabled ? "#2980b9" : "#f4f3f4"}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={toggleSwitch}
                        value={isEnabled}
                    />
                </View>
                <View
                    style={{
                        height: 50,
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <Icon name="language" size={25} style={{marginRight: 5}} />
                    <Text style={styles.textMenu}>Bahasa</Text>
                </View>
                <View
                    style={{
                        height: 50,
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <Icon name="user" size={25} style={{marginRight: 5}} />
                    <Text style={styles.textMenu}>Pengaturan Akun</Text>
                </View>
                <View
                    style={{
                        height: 50,
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <Icon name="user-times" size={25} style={{marginRight: 5}} />
                    <Text style={styles.textMenu}>Hapus Akun</Text>
                </View>
                <View
                    style={{
                        height: 50,
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <Icon name="sign-out" color='#e74c3c' size={25} style={{marginRight: 5}} />
                    <Text style={styles.textMenuKeluar}>Keluar</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    textMenu:{
        fontFamily: 'Poppins-Reguler',
        color: '#2c3e50'
    },
    textMenuKeluar:{
        fontFamily: 'Poppins-Reguler',
        color: '#c0392b'
    }
})

export default Profile