import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

// pages
import Register from './pages/Register'
import Login from './pages/Login'
import CreateRoom from './pages/CreateRoom'
import GenerateRoom from './pages/GenerateRoom'
import JoinRoom from './pages/JoinRoom'
import AfterSplash from './pages/AfterSplash';
import Home from './pages/Home'
import { LogBox } from 'react-native';

LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you\'re using an old API with gesture components, check out new Gestures system!",
]);

const Drawer = createDrawerNavigator();


const App = () => {
  return(
    <View style={{flex:1}}>
      <Home />
    </View>
  )
}

export default App;
